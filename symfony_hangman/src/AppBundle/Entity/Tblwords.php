<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tblwords
 *
 * @ORM\Table(name="tblwords", indexes={@ORM\Index(name="word_ID", columns={"word_ID"})})
 * @ORM\Entity
 */
class Tblwords
{
    /**
     * @var string
     *
     * @ORM\Column(name="word", type="string", length=100, nullable=true)
     */
    private $word;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=100, nullable=true)
     */
    private $category;

    /**
     * @var integer
     *
     * @ORM\Column(name="word_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $wordId;

    public function setID($wordId)
    {
        $this->wordId = $wordId;
    }
    public function getID()
    {
        return $this->wordId;
    }

    public function setWord($word)
    {
        $this->word = $word;
    }
    public function getWord()
    {
        return $this->word;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }
    public function getCategory()
    {
        return $this->category;
    }
}

