<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tblranking
 *
 * @ORM\Table(name="tblranking", indexes={@ORM\Index(name="rank_ID", columns={"rank_ID"})})
 * @ORM\Entity
 */
class Tblranking
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="stage", type="string", length=50, nullable=true)
     */
    private $stage;

    /**
     * @var integer
     *
     * @ORM\Column(name="rank_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $rankId;

    public function setRankId($rankId)
    {
        $this->rankId = $rankId;
    }
    public function getRankId()
    {
        return $this->rankId;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    public function getName()
    {
        return $this->name;
    }

    public function setStage($stage)
    {
        $this->stage = $stage;
    }
    public function getStage()
    {
        return $this->stage;
    }
}

