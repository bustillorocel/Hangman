<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class Hangman extends AbstractType
{
	public function buildForm(FormBuilderInterface $fbi, array $opt)
	{
		$fbi->add('name',TextType::class);
		$fbi->add('stage',TextType::class);
		$fbi->add('Submit and Retry',SubmitType::class, array('attr' => array('class' => 'btn btn-success')));
	}
}