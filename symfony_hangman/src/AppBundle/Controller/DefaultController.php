<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Tblwords;
use AppBundle\Entity\Tblranking;
use AppBundle\Form\Hangman;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
    /**
     * @Route("/Instruction")
     */
    public function instruction(Request $req)
    {
        return $this->render('instruction.html.twig');
    }
     /**
     * @Route("/Developer")
     */
    public function developer(Request $req)
    {
        return $this->render('developer.html.twig');
    }

     /**
     * @Route("/Ranking")
     */
    public function ranking(Request $req)
    {
        $repo = $this->getDoctrine()->getRepository(Tblranking::class);
        
        $rank = $repo->findAll(array('stage' => 'ASC'));

        return $this->render('ranking.html.twig',["ranking"=>$rank]);
    }
    /**
     * @Route("/Hangman", name="home")
     */
    
    public function hangman(Request $req)
    {
        $ranks = new Tblranking();
        $form = $this->createForm(Hangman::class, $ranks);
        $form->handleRequest($req);

        $newform = new Tblranking();
        $newform = $this->createForm(Hangman::class, $ranks);
        $newform->handleRequest($req);

        if($form->isSubmitted() && $form->isValid()) {
            $rank = new Tblranking();

            $data = $form->getData();

            $name = $data->getName();
            $stage = $data->getStage();
            $em = $this->getDoctrine()->getManager();
        
            $rank->setName($name);
            $rank->setStage($stage);

            $em->persist($rank);
            $em->flush();
        }
        else if($newform->isSubmitted() && $newform->isValid()){
            $rank = new Tblranking();

            $data = $form->getData();

            $name = $data->getName();
            $stage = $data->getStage();
            $em = $this->getDoctrine()->getManager();
        
            $rank->setName($name);
            $rank->setStage($stage);

            $em->persist($rank);
            $em->flush();
        }
        return $this->render('hangman.html.twig',["form"=>$form->createView(),"newform"=>$newform->createView()]);
    }

    /**
     * @Route("/randomWord", name="generate")
     */
    public function generateWord(Request $req)
    {
        $session = new Session();
        $repo = $this->getDoctrine()->getRepository(Tblwords::class);
        $id_arr = array();

        $words = $repo->findAll();
        $limit = sizeof($words);
        $num = rand(1,$limit);
        $generatedWord = $repo->findBywordId($num);
        $session->set("randomWord", $num);
        $session_len = sizeof($session->get("randomWord"));

        $word = array($generatedWord[0]->getId(),$generatedWord[0]->getWord(),$generatedWord[0]->getCategory(),$session_len,1);
        $response = new JsonResponse($word);

        return $response;
    }

    /**
     * @Route("/nextWord", name="next")
     */
    public function nextWord(Request $req)
    {
        $session = new Session();
        $repo = $this->getDoctrine()->getRepository(Tblwords::class);
        $id_arr = array();

        $words = $repo->findAll();
        $limit = sizeof($words);
        $num = rand(1,$limit);
        $id = $session->get('randomWord');
    
        if(empty($id)) {
            $session->set("randomWord", $num);
            $id_arr[0] = 1;
        }
        else {
            $session->set("randomWord",($id . ',' .  $num));
            $id_arr = explode(',', $session->get('randomWord'));
        }
        $id_arr_len = sizeof($id_arr);
        $generatedWord = $repo->findBywordId($num);
        
        $word = array($generatedWord[0]->getId(),$generatedWord[0]->getWord(),$generatedWord[0]->getCategory(),$id_arr_len);
        $response = new JsonResponse($word);

        return $response;
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/session")
     */
    public function session(Request $req)
    {
        $session = new Session();
        $id = $session->get('randomWord');
        $words = array($id, sizeof(explode(',',$id)));
        $response = new JsonResponse($words);
        return $response;
    }

    /**
     * @Route("/unset")
     */
    public function unset(Request $req)
    {
        $session = new Session();
        $session->remove('randomWord');
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/Ajax/allWord")
     */
    public function allWord(Request $req)
    {
        $repo = $this->getDoctrine()->getRepository(Tblwords::class);
        
        $prod = $repo->findAll();
        $len = sizeof($prod);
        $word = array();
        for($i=0;$i<$len;$i++) {
            $word[$i][0] = $prod[$i]->getId();
            $word[$i][1] = $prod[$i]->getWord();
            $word[$i][2] = $prod[$i]->getCategory();
        }
        $response = new JsonResponse($word);
        return $response;
    }
}
