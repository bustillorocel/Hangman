/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.21-MariaDB : Database - dbhangman
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbhangman` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dbhangman`;

/*Table structure for table `tblranking` */

DROP TABLE IF EXISTS `tblranking`;

CREATE TABLE `tblranking` (
  `rank_ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `stage` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`rank_ID`),
  KEY `rank_ID` (`rank_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `tblranking` */

insert  into `tblranking`(`rank_ID`,`name`,`stage`) values (1,'Hangaroo Master','10'),(2,'Hangaroo ','9'),(3,'Hangaroo ','8'),(4,'Hangaroo ','7'),(5,'Hangaroo ','6'),(6,'Hangaroo ','5'),(7,'Hangaroo ','4'),(8,'Hangaroo ','3'),(9,'Hangaroo ','2'),(10,'Hangaroo ','1'),(12,'rocel','3'),(13,'rocel','3'),(14,'rocel','3'),(15,'Rocel','10');

/*Table structure for table `tblwords` */

DROP TABLE IF EXISTS `tblwords`;

CREATE TABLE `tblwords` (
  `word_ID` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`word_ID`),
  KEY `word_ID` (`word_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Data for the table `tblwords` */

insert  into `tblwords`(`word_ID`,`word`,`category`) values (1,'Hall of Fame The Script','Song/Singer'),(2,'Elephant','Animals'),(3,'Philippines','Country'),(4,'Bruised and Scarred Mayday Parade','Song/Singer'),(5,'Sinigang','Food'),(6,'Lumberjack','Profession'),(7,'Hamburger','Food'),(8,'Rainbow Six Siege','Online Game'),(9,'Fever','Sickness'),(10,'Chicken','Animals'),(11,'Pakistan','Country'),(12,'Nicole Kidman','Actor/Actress'),(13,'Despacito Luis Fonci','Song/Singer'),(14,'Will Smith','Actor/Actress'),(15,'Airplane','Vehicle'),(16,'Isaac Newtown','Scientist'),(17,'Albert Einstien','Scientist'),(18,'Nikola Tesla','Scientist'),(19,'Maps Maroon Five','Song/Singer'),(20,'My Heart Will Go On Celine Dion ','Song/Singer'),(21,'Kundiman Silent Sanctuary','Song/Singer'),(22,'Llama','Animals'),(23,'Tiger','Animals'),(24,'Giraffe','Animals'),(25,'French Fries','Food'),(26,'Lasagna','Food'),(27,'Barbeque','Food'),(28,'Baseball','Sport'),(29,'Programmer','Profession'),(30,'Basketball','Sport');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
