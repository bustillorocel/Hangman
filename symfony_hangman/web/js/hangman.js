$(document).ready(function() {
	var word = Array();
	var lives = 4;
	var answer;
	var stage = 1;
	var url = '/randomWord';
	letters = Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
	
	$.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: {
        },
        success: function(data) {
        	len = data[1].length;
        	word = data;
        	answer = len;
        	for(i=0;i<len;i++) {
        		if(data[1][i] != ' ') {
        			label = '<label id="letter_' + i + '" class="blank"> </label>';
        		}
        		else {
        			label='<label class="space"> </label>';
        			answer--;
        		}
    			$('#hangman').append(label);
    			$('#level').text('Level ' + data[3]);
    			$('#category').text(data[2]);
        	}
        },
        error: function(){
        }
	});

	$('#show').click(function() {
		$('#complete_modal').modal('show');
	});

	$('#next_level').click(function() {
		$('#hangman').html('');
		$('#congrats_modal').modal('hide');
		if(stage == 2) {
			$('#hangman_stage').val(data[3]);
			$('#complete_modal').modal('show');
		}
		else {
			$.ajax({
		        url: '/nextWord',
		        type: 'POST',
		        dataType: 'json',
		        data: {
		        },
		        success: function(data) {
		        	lives = 4;
		        	len = data[1].length;
		        	word = data;
		        	answer = len;
		        	stage++;
		        	for(i=0;i<len;i++) {
		        		if(data[1][i] != ' ') {
		        			label = '<label id="letter_' + i + '" class="blank"> </label>';
		        		}
		        		else {
		        			label='<label class="space"> </label>';
		        			answer--;
		        		}
		    			$('#hangman').append(label);
						$('#level').text('Level ' + data[3]);
		    			$('#category').text(data[2]);
						$('.key').removeAttr('disabled');
						for(j=1; j<=4;j++) {
							$('#live' + j).removeClass('active-live');
							$('#live' + j).addClass('lives');
						}
		        	}
		        },
		        error: function(){
		        }
			});
		}
	});

	for(i=0;i<letters.length;i++) {
		$('#btn' + letters[i]).click(function() {
			var flag = false;
			$(this).attr('disabled','disabled');
			letter = $(this).text();
			len = word[1].length;

			for(i = 0;i < len;i++) {
				if(word[1][i].toUpperCase() == letter) {
					$('#letter_' + i).text(letter);
					flag = true;
					answer--;
				}
			}
			if(flag == false) {
				lives--;
				$('#live' + (lives + 1)).removeClass('lives');
				$('#live' + (lives + 1)).addClass('active-live');

				if(lives == 0) {
					level = $('#level').text();
					$('#view_answer').text('The Word is: ' + word[1]);
					$('#hangman_stage').val(level);
					$('#gameover_modal').modal('show');
					$('.key').attr('disabled','disabled');
				}
			}
			else {
				if(answer == 0) {
					$('#congrats_modal').modal('show');
					$('.key').attr('disabled','disabled');
				}
			}
		});
	}
});